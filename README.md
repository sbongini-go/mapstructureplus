# mapstructureplus

Fork del progetto [mapstructure](https://github.com/mitchellh/mapstructure) versione v1.4.3.0

## Funzionalita aggiuntive
Di seguito le funzionalita aggiuntive rispetto a **mapstructure**:
1. E' possibile fare il decode di strutture time.Duration nel formato 3s, 5m, 4h, ecc..
2. E' possibile fare il decode dentro campi privati utilizzando il metodo `PrivateDecode`.
    Utilizzando il metodo `Decode` i campi privati invece vengono ignorati.



